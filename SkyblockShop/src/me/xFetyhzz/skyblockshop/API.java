package me.xFetyhzz.skyblockshop;

import org.bukkit.entity.Player;

public class API {

	public static void giveCoin(Player p, int i) {
		Shop.config.set(p.getName() + ".Coin",
				Shop.config.getInt(p.getName() + ".Coin", 0) + i);
		Shop.saveFile();
		p.sendMessage("�2" + i + " coin received!");
	}

	public static void takeCoin(Player p, int i) {
		Shop.config.set(p.getName() + ".Coin",
				Shop.config.getInt(p.getName() + ".Coin", 0) - i);
		Shop.saveFile();
		p.sendMessage("�c" + i + " coin taken!");
	}

	public static boolean hasEnough(Player p, int i) {
		if (Shop.config.getInt(p.getName() + ".Coin") >= i)
			return true;
	
		return false;
	}


}

