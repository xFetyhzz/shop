package me.xFetyhzz.skyblockshop;


import org.bukkit.Material;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;


public class EventHandlers implements Listener {
	
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		
		
		if (!Shop.config.contains(p.getName())) {
			Shop.config.set(p.getName() + ".Coin", 0);
		}
	}

	@EventHandler
	public void onKill(EntityDeathEvent e) {

		if (e.getEntity() instanceof Monster) {
			Monster m = (Monster) e.getEntity();
			if (m.getKiller() instanceof Player) {
				Player p = m.getKiller();

				API.giveCoin(p, 2);
			Shop.saveFile();
				
					
					
			}
		}
	}
	
	


	@EventHandler
	public void onClick(InventoryClickEvent event) {
		Player p = (Player) event.getWhoClicked();
		
			if (event.getInventory().getName().equals(Shop.shop.getName())) {
				event.setCancelled(true);

			if (event.getCurrentItem() == null) {
				return;
			}

			if (!(event.getCurrentItem().hasItemMeta())) {
				return;
			}

			if (event.getCurrentItem().getItemMeta().getDisplayName()
					.equals("�3 2 Apples!")) {
				if (API.hasEnough(p, 2)) {
					API.takeCoin(p, 2);
					p.getInventory().addItem(new ItemStack(Material.APPLE, 2));
				} else {
					p.sendMessage("�cYou need more Coins!");
				}
			}
				
				
			if (event.getCurrentItem().getItemMeta().getDisplayName()
					.equals("�3 4 Bread!")) {
				if (API.hasEnough(p, 5)) {
					API.takeCoin(p, 5);
					p.getInventory().addItem(new ItemStack(Material.BREAD, 4));
				} else {
					p.sendMessage("�cYou need more Coins!");
					
						}
					}
				
			if (event.getCurrentItem().getItemMeta().getDisplayName()
					.equals("�3 5 Beef!")) {
				if (API.hasEnough(p, 7)) {
					API.takeCoin(p, 7);
					p.getInventory().addItem(new ItemStack(Material.COOKED_BEEF, 5));
				} else {
					p.sendMessage("�cYou need more Coins!");
					
				}
				}
					if (event.getCurrentItem().getItemMeta().getDisplayName()
							.equals("�3 3 Potato!")) {
						if (API.hasEnough(p, 3)) {
							API.takeCoin(p, 3);
							p.getInventory().addItem(new ItemStack(Material.POTATO_ITEM, 3));
						} else {
							p.sendMessage("�cYou need more Coins!");
						}
						}
					
					if (event.getCurrentItem().getItemMeta().getDisplayName()
							.equals("�3 1 Pumpkin Pie!")) {
						if (API.hasEnough(p, 5)) {
							API.takeCoin(p, 5);
							p.getInventory().addItem(new ItemStack(Material.PUMPKIN_PIE, 1));
						} else {
							p.sendMessage("�cYou need more Coins!");
							
						}
						}
					
					if (event.getCurrentItem().getItemMeta().getDisplayName()
							.equals("�3 10 Carrot!")) {
						if (API.hasEnough(p, 12)) {
							API.takeCoin(p, 12);
							p.getInventory().addItem(new ItemStack(Material.CARROT_ITEM, 10));
						} else {
							p.sendMessage("�cYou need more Coins!");
						}
						}
					
					if (event.getCurrentItem().getItemMeta().getDisplayName()
							.equals("�3 2 Cake!")) {
						if (API.hasEnough(p, 7)) {
							API.takeCoin(p, 7);
							p.getInventory().addItem(new ItemStack(Material.CAKE, 2));
						} else {
							p.sendMessage("�cYou need more Coins!");
						}
						}
					
					
					if (event.getCurrentItem().getItemMeta().getDisplayName()
							.equals("�3 15 Cookie!")) {
						if (API.hasEnough(p, 12)) {
							API.takeCoin(p, 12);
							p.getInventory().addItem(new ItemStack(Material.COOKIE, 15));
						} else {
							p.sendMessage("�cYou need more Coins!");
						}
						}
					
					if (event.getCurrentItem().getItemMeta().getDisplayName()
							.equals("�3 5 Fish!")) {
						if (API.hasEnough(p, 6)) {
							API.takeCoin(p, 6);
							p.getInventory().addItem(new ItemStack(Material.RAW_FISH, 5));
						} else {
							p.sendMessage("�cYou need more Coins!");
						}
						}
					
						
					
		if (event.getCurrentItem().getItemMeta().getDisplayName()
				.equals("�3 16 Sandstone!")) {
			if (API.hasEnough(p, 16)) {
				API.takeCoin(p, 16);
				p.getInventory().addItem(new ItemStack(Material.SANDSTONE, 16));
			} else {
				p.sendMessage("�cYou need more Coins!");
			}
			}
		
		if (event.getCurrentItem().getItemMeta().getDisplayName()
				.equals("�3 16 Cobblestone!")) {
			if (API.hasEnough(p, 13)) {
				API.takeCoin(p, 13);
				p.getInventory().addItem(new ItemStack(Material.COBBLESTONE, 16));
			} else {
				p.sendMessage("�cYou need more Coins!");
			}
			}
		if (event.getCurrentItem().getItemMeta().getDisplayName()
				.equals("�3 16 Stone!")) {
			if (API.hasEnough(p, 18)) {
				API.takeCoin(p, 18);
				p.getInventory().addItem(new ItemStack(Material.STONE, 16));
			} else {
				p.sendMessage("�cYou need more Coins!");
			}
			}
		if (event.getCurrentItem().getItemMeta().getDisplayName()
				.equals("�3 16 Dirt!")) {
			if (API.hasEnough(p, 22)) {
				API.takeCoin(p, 22);
				p.getInventory().addItem(new ItemStack(Material.DIRT, 16));
			} else {
				p.sendMessage("�cYou need more Coins!");
			}
			}
		if (event.getCurrentItem().getItemMeta().getDisplayName()
				.equals("�3 16 Sand!")) {
			if (API.hasEnough(p, 17)) {
				API.takeCoin(p, 17);
				p.getInventory().addItem(new ItemStack(Material.SAND, 16));
			} else {
				p.sendMessage("�cYou need more Coins!");
			}
			}
		
		if (event.getCurrentItem().getItemMeta().getDisplayName()
				.equals("�3 16 Wood!")) {
			if (API.hasEnough(p, 20)) {
				API.takeCoin(p, 20);
				p.getInventory().addItem(new ItemStack(Material.WOOD, 16));
			} else {
				p.sendMessage("�cYou need more Coins!");
			}
			}
		if (event.getCurrentItem().getItemMeta().getDisplayName()
				.equals("�3 16 Clay!")) {
			if (API.hasEnough(p, 24)) {
				API.takeCoin(p, 24);
				p.getInventory().addItem(new ItemStack(Material.CLAY, 16));
			} else {
				p.sendMessage("�cYou need more Coins!");
			}
			}
		
		if (event.getCurrentItem().getItemMeta().getDisplayName()
				.equals("�3 16 Wool!")) {
			if (API.hasEnough(p, 22)) {
				API.takeCoin(p, 22);
				p.getInventory().addItem(new ItemStack(Material.WOOL, 16));
			} else {
				p.sendMessage("�cYou need more Coins!");
			}
			}
		
		if (event.getCurrentItem().getItemMeta().getDisplayName()
				.equals("�3 16 Bricks!")) {
			if (API.hasEnough(p, 25)) {
				API.takeCoin(p, 25);
				p.getInventory().addItem(new ItemStack(Material.BRICK, 16));
			} else {
				p.sendMessage("�cYou need more Coins!");
			}
			}
		
		if (event.getCurrentItem().getItemMeta().getDisplayName()
				.equals("�3 Sell stuff!")) {
			if (API.hasEnough(p, 0)) {
				
				p.chat("/sell");
			} else {
				p.sendMessage("�c Can't open /sell!");
			}
			}
		
		if (event.getCurrentItem().getItemMeta().getDisplayName()
				.equals("�3 More comming soon!")) {
			if (API.hasEnough(p, 0)) {
				
				
			} else {
				
			}
			}
		
		if (event.getCurrentItem().getItemMeta().getDisplayName()
				.equals("�3 Exit!")) {
			if (API.hasEnough(p, 0)) {
				
				p.closeInventory();
			} else {
				p.sendMessage("�c Can't close inventory!");
			}
			

			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			
			
			
				
				
			
			}
		}
	}
}

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//                                 
//
//
//
//
//
//
//
//
//
//


