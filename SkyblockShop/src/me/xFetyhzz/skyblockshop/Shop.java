package me.xFetyhzz.skyblockshop;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

public class Shop extends JavaPlugin implements Listener {

	public static FileConfiguration config;

	public static Shop plugin = null;

	
	public void onEnable() {
		getServer().getPluginManager().registerEvents(new EventHandlers(), this);	
		getServer().getPluginManager().registerEvents(new EventSell(), this);


		config = getConfig();

		plugin = this;
	}
	
	
	public static void saveFile(){
		plugin.saveConfig();
	}

	public ItemStack createItem(Material material, int amount, short shrt,
			String displayname, String lore) {
		ItemStack item = new ItemStack(material, amount, (short) shrt);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(displayname);
		ArrayList<String> Lore = new ArrayList<String>();
		Lore.add(lore);
		meta.setLore(Lore);

		item.setItemMeta(meta);
		return item;
	}

	static Inventory sell;
	 
	{
		sell = Bukkit.createInventory(null, 36, "�6Skyblock Shop - Sell");
		
		
		
		
		sell.setItem(
				0,
				createItem(Material.APPLE, 1, (short) 0, "�3 2 Apples!",
						"�fGetback: �61 �fCoin"));
		
		sell.setItem(
				1,
				createItem(Material.BREAD, 1, (short) 0, "�3 4 Bread!",
						"�fGetback: �65 �fCoins"));
		
		sell.setItem(
				2,
				createItem(Material.COOKED_BEEF, 1, (short) 0, "�3 5 Beef!",
						"�fGetback: �67 �fCoins"));
		
		sell.setItem(
				3,
				createItem(Material.POTATO_ITEM, 1, (short) 0, "�3 3 Potato!",
						"�fGetback: �65 �fCoins"));
		sell.setItem(
				4,
				createItem(Material.PUMPKIN_PIE, 1, (short) 0, "�3 1 Pumpkin Pie!",
						"�fGetback: �65 �fCoins"));
		sell.setItem(
				5,
				createItem(Material.CARROT_ITEM, 1, (short) 0, "�3 10 Carrot!",
						"�fGetback: �612 �fCoins"));
		sell.setItem(
				6,
				createItem(Material.CAKE, 1, (short) 0, "�3 2 Cake!",
						"�fGetback: �67 �fCoins"));
		sell.setItem(
				7,
				createItem(Material.COOKIE, 1, (short) 0, "�3 15 Cookie!",
						"�fGetback: �612 �fCoins"));
		sell.setItem(
				8,
				createItem(Material.RAW_FISH, 1, (short) 0, "�3 5 Fish!",
						"�fGetback: �66 �fCoins"));
		sell.setItem(
				9,
				createItem(Material.SANDSTONE, 1, (short) 0, "�3 16 Sandstone!",
						"�fGetback: �616 �fCoins"));
		sell.setItem(
				10,
				createItem(Material.COBBLESTONE, 1, (short) 0, "�3 16 Cobblestone!",
						"�fGetback: �613 �fCoins"));
		sell.setItem(
				11,
				createItem(Material.STONE, 1, (short) 0, "�3 16 Stone!",
						"�fGetback: �618 �fCoins"));
		sell.setItem(
				12,
				createItem(Material.DIRT, 1, (short) 0, "�3 16 Dirt!",
						"�fGetback: �622 �fCoins"));
		sell.setItem(
				13,
				createItem(Material.SAND, 1, (short) 0, "�3 16 Sand!",
						"�fGetback: �617 �fCoins"));
		sell.setItem(
				14,
				createItem(Material.WOOD, 1, (short) 0, "�3 16 Wood!",
						"�fGetback: �620 �fCoins"));
		sell.setItem(
				15,
				createItem(Material.CLAY, 1, (short) 0, "�3 16 Clay!",
						"�fGetback: �624 �fCoins"));
		sell.setItem(
				16,
				createItem(Material.WOOL, 1, (short) 0, "�3 16 Wool!",
						"�fGetback: �622 �fCoins"));
		sell.setItem(
				17,
				createItem(Material.BRICK, 1, (short) 0, "�3 16 Bricks!",
						"�fGetback: �612 �fCoins"));
		sell.setItem(
				27,
				createItem(Material.EMERALD, 1, (short) 0, "�3 Exit!",
						""));
		sell.setItem(
				35,
				createItem(Material.EMERALD, 1, (short) 0, "�3 More comming soon!",
						""));
		
		
	}
	static Inventory shop;

	{
		shop = Bukkit.createInventory(null, 36, "�6Skyblock Shop - Buy");

		shop.setItem(
				0,
				createItem(Material.APPLE, 1, (short) 0, "�3 2 Apples!",
						"�fPrice �62 �fCoins"));
		
		shop.setItem(
				1,
				createItem(Material.BREAD, 1, (short) 0, "�3 4 Bread!",
						"�fPrice �65 �fCoins"));
		
		shop.setItem(
				2,
				createItem(Material.COOKED_BEEF, 1, (short) 0, "�3 5 Beef!",
						"�fPrice �67 �fCoins"));
		
		shop.setItem(
				3,
				createItem(Material.POTATO_ITEM, 1, (short) 0, "�3 3 Potato!",
						"�fPrice �65 �fCoins"));
		shop.setItem(
				4,
				createItem(Material.PUMPKIN_PIE, 1, (short) 0, "�3 1 Pumpkin Pie!",
						"�fPrice �65 �fCoins"));
		shop.setItem(
				5,
				createItem(Material.CARROT_ITEM, 1, (short) 0, "�3 10 Carrot!",
						"�fPrice �612 �fCoins"));
		shop.setItem(
				6,
				createItem(Material.CAKE, 1, (short) 0, "�3 2 Cake!",
						"�fPrice �67 �fCoins"));
		shop.setItem(
				7,
				createItem(Material.COOKIE, 1, (short) 0, "�3 15 Cookie!",
						"�fPrice �612 �fCoins"));
		shop.setItem(
				8,
				createItem(Material.RAW_FISH, 1, (short) 0, "�3 5 Fish!",
						"�fPrice �66 �fCoins"));
		shop.setItem(
				9,
				createItem(Material.SANDSTONE, 1, (short) 0, "�3 16 Sandstone!",
						"�fPrice �616 �fCoins"));
		shop.setItem(
				10,
				createItem(Material.COBBLESTONE, 1, (short) 0, "�3 16 Cobblestone!",
						"�fPrice �613 �fCoins"));
		shop.setItem(
				11,
				createItem(Material.STONE, 1, (short) 0, "�3 16 Stone!",
						"�fPrice �618 �fCoins"));
		shop.setItem(
				12,
				createItem(Material.DIRT, 1, (short) 0, "�3 16 Dirt!",
						"�fPrice �622 �fCoins"));
		shop.setItem(
				13,
				createItem(Material.SAND, 1, (short) 0, "�3 16 Sand!",
						"�fPrice �617 �fCoins"));
		shop.setItem(
				14,
				createItem(Material.WOOD, 1, (short) 0, "�3 16 Wood!",
						"�fPrice �620 �fCoins"));
		shop.setItem(
				15,
				createItem(Material.CLAY, 1, (short) 0, "�3 16 Clay!",
						"�fPrice �624 �fCoins"));
		shop.setItem(
				16,
				createItem(Material.WOOL, 1, (short) 0, "�3 16 Wool!",
						"�fPrice �622 �fCoins"));
		shop.setItem(
				17,
				createItem(Material.BRICK, 1, (short) 0, "�3 16 Bricks!",
						"�fPrice �625 �fCoins"));
		shop.setItem(
				27,
				createItem(Material.EMERALD, 1, (short) 0, "�3 Exit!",
						""));
		shop.setItem(
				35,
				createItem(Material.EMERALD, 1, (short) 0, "�3 Sell stuff!",
						""));

		
	}
	
	// /BUY EFFECT 
	public void playCircularEffect(org.bukkit.Location location, Effect effect, boolean v)	{
		for(int i = 0; i<=8; i+= ((!v && (i==3)) ? 2 : 1))
			location.getWorld().playEffect(location, effect, i);
	}

	// COMMAND /BUY || /SELL

	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] a) {
		Player player = (Player) sender;
		if (cmd.getName().equalsIgnoreCase("buy")) {
			player.openInventory(shop);
				playCircularEffect(((Player)sender).getLocation(), Effect.ENDER_SIGNAL, true);					
		}		
		
		if (cmd.getName().equalsIgnoreCase("sell")) {
			player.openInventory(sell);
				playCircularEffect(((Player)sender).getLocation(), Effect.ENDER_SIGNAL, true);					
		} 
		
		
		return true;
			
		}
		
	}


			
